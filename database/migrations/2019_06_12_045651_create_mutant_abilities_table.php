<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMutantAbilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mutant_abilities', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('mutant_id');
            $table->foreign('mutant_id')->references('id')->on('mutants')->onDelete('cascade');

            $table->unsignedBigInteger('ability_id');
            $table->foreign('ability_id')->references('id')->on('abilities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mutant_abilities');
    }
}
