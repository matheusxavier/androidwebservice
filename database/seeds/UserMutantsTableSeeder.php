<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Mutant;
use App\Models\Ability;

class UserMutantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Matheus',
            'email' => 'matheusbergam@hotmail.com',
            'password' => Hash::make('123456')
        ]);

        for ($i = 1; $i <= 10; $i++) {
            $mutant = Mutant::create([
                'name' => "Mutante $i",
                'user_id' => $user->id,
                'image_path' => "teste.png"
            ]);

            for ($j = 1; $j <= 3; $j++) {
                $ability = Ability::create([
                    'name' => "Habilidade $j"
                ]);
                $mutant->abilities()->save($ability);
            }
        }

        User::create([
            'name' => 'User Teste 1',
            'email' => 'teste1@hotmail.com',
            'password' => Hash::make('123456')
        ]);

        User::create([
            'name' => 'User Teste 2',
            'email' => 'teste2@hotmail.com',
            'password' => Hash::make('123456')
        ]);
    }
}
