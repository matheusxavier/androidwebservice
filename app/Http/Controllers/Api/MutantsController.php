<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Mutant;
use App\Models\Ability;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MutantsController extends Controller
{
    public function store(Request $request)
    {

        try {
            DB::beginTransaction();

            $request->validate([
                'name' => 'required|unique:mutants',
                'abilities' => 'required',
                'image_path' => 'required'
            ]);

            $fileName = "public/mutants/" . time() . "_mutant.jpeg";

            Storage::put($fileName, base64_decode($request->post('image_path')), 'public');

            $request->merge(['image_path' => $fileName, 'user_id' => auth()->user()->id]);

            $mutant = Mutant::create($request->only(['name', 'image_path', 'user_id']));
            foreach ($request->post('abilities') as $ability) {
                $model = Ability::create(['name' => $ability['name']]);
                $mutant->abilities()->save($model);
            }

            DB::commit();
            return response()->json($mutant, 201);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => 'Erro ao salvar mutante.'], 500);
        }
    }

    public function index()
    {
        return response()->json(Mutant::get(), 200);
    }

    public function listMyMutants()
    {
        $mutants = auth()->user()->mutants()->with('abilities')->get();
        return response()->json($mutants, 200);
    }

    public function search(Request $request)
    {
        $term = $request->get('term');

        $mutants = auth()->user()->mutants()->with('abilities')->where('name', 'like', "%$term%")
            ->orWhereHas('abilities', function($query) use ($term) {
                $query->where('name', 'like', "%$term%");
            })->get();

        return response()->json($mutants, 200);
    }

    public function destroy($id)
    {
        $mutant = Mutant::findOrFail($id);
        $mutant->delete();

        return response()->json(['msg' => 'Mutante excluido com sucesso!'], 200);
    }
}
