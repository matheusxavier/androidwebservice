<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Mutant extends Model
{
    protected $fillable = [
        'name', 'image_path', 'user_id',
    ];

    public function getImagePathAttribute($value)
    {
        return Storage::url($value);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function abilities()
    {
        return $this->belongsToMany(\App\Models\Ability::class, 'mutant_abilities');
    }
}
