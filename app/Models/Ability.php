<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ability extends Model
{
    protected $fillable = [
        'name',
    ];

    public function mutants()
    {
        return $this->belongsTo(App\Models\Mutant::class, 'mutant_abilities');
    }
}
